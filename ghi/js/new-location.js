// 1 "We need to add an event listener for when the DOM loads."
// 2 "Let's declare a variable that will hold the URL for the API that we just created."
// 3 "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// 4 "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const selector = document.getElementById("state")

      for (let state of data.states) {
        const stateValue = document.createElement("option")
        stateValue.value = state.abbreviation
        stateValue.innerHTML = state.name
        selector.appendChild(stateValue)
      }
      const formTag = document.getElementById('create-location-form')
      formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        try{
            const locationUrl = 'http://localhost:8000/api/locations/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const response = await fetch(locationUrl, fetchConfig)
            if (response.ok) {
                formTag.reset()
                const newLoaction = await response.json()
                console.log(newLoaction)

            }
        } catch(err) {
             console.log(err)
        }
      })
    }

});
// we want to be able to select the states INSIDE our form on localhost:3000/
// get element by id = state
