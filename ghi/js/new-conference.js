// This will get the list of states that we created event_api_views

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/'
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json()
        // insomnia response full of conferences
        // console.log(data)
        // to get the conferences consoled data
        //
        const selector = document.getElementById('location')

        for(let location of data.locations){
            // This code is to put data in the drop down menu for "choose a location"
            const locationValue = document.createElement('option'); // 'option' because that is what it is called in the HTML file
            locationValue.value = location.id
            // From Insomnia see that conference object has name @ location
            locationValue.innerHTML = location.name
            // Append the option element as a child of the select tag
            selector.appendChild(locationValue)

        }
        const formTag = document.getElementById('create-conference-form')
        // Make the page not refresh when form is submitted
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            //create FormData object from form element (aka all the input data start, end, max, desc)
            const formData = new FormData(formTag)
            // turning FormData object into json
            const json = JSON.stringify(Object.fromEntries(formData))
            try {
                const conferenceUrl = 'http://localhost:8000/api/conferences/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
                // awaiting the fetch from location URL
                const response = await fetch(conferenceUrl, fetchConfig)
                if (response.ok) {
                    formTag.reset()
                    // create the new location and save it in database/insomnia
                    const newLocation = await response.json()

                }
            } catch(e){
                console.log(e)
            }
        })

    }
})

// We want to be able to select the states INSIDE the drop down menu!
// get element by id = state
