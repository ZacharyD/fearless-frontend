window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      const loadingIcon = document.getElementById('loading-conference-spinner')
      loadingIcon.classList.add('d-none')
      selectTag.classList.remove('d-none')

    }
    const formTag = document.getElementById('create-attendee-form')
        // Make the page not refresh when form is submitted
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            //create FormData object from form element (aka all the input data start, end, max, desc)
            const formData = new FormData(formTag)
            // turning FormData object into json
            const json = JSON.stringify(Object.fromEntries(formData))
            try {
                const attendeeUrl = 'http://localhost:8001/api/attendees/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
                // awaiting the fetch from location URL
                const response = await fetch(attendeeUrl, fetchConfig)
                if (response.ok) {
                    formTag.reset()
                    // create the new location and save it in database/insomnia
                    const newAttendee = await response.json()
                    console.log(newAttendee)

                    const successAlert = document.getElementById('success-message')
                    successAlert.classList.remove('d-none')

                    formTag.classList.add('d-none')

                }
            } catch(e){
                console.log(e)
            }
        })
})
